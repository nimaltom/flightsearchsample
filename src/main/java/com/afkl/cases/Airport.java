package com.afkl.cases;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Airport {

	
	private String code;
	private String name;
	private String description;
	private Map<String,String> coordinates;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Map<String, String> getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(Map<String, String> coordinates) {
		this.coordinates = coordinates;
	}
	
}

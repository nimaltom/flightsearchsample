package com.afkl.cases.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.afkl.cases.Airport;
import com.afkl.cases.Fare;

@RestController
public class FlightSearchController {

	private String token = null;

	@RequestMapping(value = "/airports", method = RequestMethod.GET)
	public List<Airport> getAirportList() {
		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> params = new HashMap<String, String>();
		params.put("client-id", "travel-api-client");
		params.put("secret", "psw");
		params.put("grantType", "client_credentials");
		token = restTemplate.getForObject("http://localhost:8080/oauth/token", String.class);

		ResponseEntity<List<Airport>> responseEntity = restTemplate.exchange(
				"http://localhost:8080/airports?access_token=" + token, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Airport>>() {
				});
		List<Airport> objects = responseEntity.getBody();
		MediaType contentType = responseEntity.getHeaders().getContentType();
		HttpStatus statusCode = responseEntity.getStatusCode();
		return objects;
	}

	@RequestMapping(value = "/airports/{code}", method = RequestMethod.POST)
	public Airport getAirport(@RequestParam(value = "code") String code) {
		RestTemplate restTemplate = new RestTemplate();
		Airport airport = restTemplate.getForObject("http://localhost:8080/airports/" + code + "?access_token=" + token,
				Airport.class);
		return airport;
	}

	@RequestMapping(value = "/fares/{origin}/{destination}", method = RequestMethod.POST)
	public Fare greeting(@PathVariable String origin, @PathVariable String destination) {
		RestTemplate restTemplate = new RestTemplate();
		Fare fare = restTemplate.getForObject("http://localhost:8080/fares/AMS/CDG?access_token=" + token, Fare.class);
		return fare;
	}
}
